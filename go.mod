module gitlab.com/ibnishak/gohtmlorg

go 1.12

require (
	github.com/mattn/go-runewidth v0.0.4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
)
